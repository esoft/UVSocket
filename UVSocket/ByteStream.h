////////////////////////////////////////////////////////////////////////
// Copyright(c) 1999-2016, All Rights Reserved
// Author:	FU YAN
// Created:2016/01/11
// Describe:ByteStream
////////////////////////////////////////////////////////////////////////
#include <string>
#include "UVSocketDef.h"

#pragma warning(push)
#pragma warning(disable:4200)
typedef struct ByteStreamHead_s
{
	uint16_t usAction;			//消息类型
	uint16_t usBodySize;				//消息体的长度
	char szBuf[0];
}ByteStreamHead_t;
#pragma warning(pop)

class CByteStream
{
public:
	CByteStream();
	CByteStream(const CByteStream &c); // Copy constructor
	CByteStream &operator=(const CByteStream &c); // Copy assignment operator
	~CByteStream();

	void Clear();
	const char* GetBufferPointer() const;
	int GetCapacity() const;
	uint16_t GetSize() const;
	uint16_t GetBodySize() const;
	bool WriteBytes(const char* pBuf, int nSize);
	int ReadBytes(char* pDstBuf, int nSize);
	void Dump();
	uint16_t GetAction() const;
	void SetAction(uint16_t usAction);

	// Operator << overloading
	CByteStream &operator<<(const bool val);
	CByteStream &operator<<(const char val);
	CByteStream &operator<<(const uint8_t val);
	CByteStream &operator<<(const int8_t val);
	CByteStream &operator<<(const uint16_t val);
	CByteStream &operator<<(const int16_t val);
	CByteStream &operator<<(const uint32_t val);
	CByteStream &operator<<(const int32_t val);
	CByteStream &operator<<(const unsigned long val);
	CByteStream &operator<<(const long val);
	CByteStream &operator<<(const int64_t val);
	CByteStream &operator<<(const uint64_t val);
	CByteStream &operator<<(const float val);
	CByteStream &operator<<(const double val);
	CByteStream &operator<<(const long double val);
	CByteStream &operator<<(const char* szStr);
	CByteStream &operator<<(const std::string str);

	// Operator >> overloading
	CByteStream &operator>>(bool& val);
	CByteStream &operator>>(char& val);
	CByteStream &operator>>(uint8_t& val);
	CByteStream &operator>>(int8_t& val);
	CByteStream &operator>>(uint16_t& val);
	CByteStream &operator>>(int16_t& val);
	CByteStream &operator>>(uint32_t& val);
	CByteStream &operator>>(int32_t& val);
	CByteStream &operator>>(unsigned long& val);
	CByteStream &operator>>(long& val);
	CByteStream &operator>>(uint64_t& val);
	CByteStream &operator>>(int64_t& val);
	CByteStream &operator>>(float& val);
	CByteStream &operator>>(double& val);
	CByteStream &operator>>(long double& val);
	CByteStream &operator>>(char* szStr);
	CByteStream &operator>>(std::string& str);

private:
	bool Extend(int nNewSize = 0);

private:
	char *m_pBuffer;
	int m_nCapacity;
	int m_nReadIndex;
	ByteStreamHead_t* m_pHead;
};