////////////////////////////////////////////////////////////////////////
// Copyright(c) 1999-2016, All Rights Reserved
// Author:	FU YAN
// Created:2016/01/05
// Describe:
////////////////////////////////////////////////////////////////////////
#include "UVSocketCallBack.h"
#include "TestServer.h"
#include "vld.h"
#include "time.h"
#include "ByteStream.h"
#include <string>

void TestServerSync()
{
	CTestServer objServer;
	CUVServerSocket* pServerSocket = CUVServerSocket::CreateNew();
	if (pServerSocket == NULL || !pServerSocket->Create(&objServer))
	{
		SAFE_RELEASE(pServerSocket);
		return;
	}

	UVSocketAddress objAddr;
	objAddr.strIP = "0.0.0.0";
	objAddr.nPort = 5816;
	pServerSocket->Listen(&objAddr);
	pServerSocket->SetRunMode(SOCKET_RUN_SYNC);

	time_t tBegin = time(NULL);
	while (time(NULL) - tBegin < 10)
	{
		pServerSocket->Run();
	}

	SAFE_RELEASE(pServerSocket);
}

void TestServerAsync()
{
	CTestServer objServer;
	CUVServerSocket* pServerSocket = CUVServerSocket::CreateNew();
	if (pServerSocket == NULL || !pServerSocket->Create(&objServer))
	{
		SAFE_RELEASE(pServerSocket);
		return;
	}

	UVSocketAddress objAddr;
	objAddr.strIP = "0.0.0.0";
	objAddr.nPort = 5816;
	pServerSocket->Listen(&objAddr);
	pServerSocket->Run();
	Sleep(10000);

	SAFE_RELEASE(pServerSocket);
}

void TestByteStream()
{
	CByteStream bs;

	bool a = false;
	char szText[32] = "我爱你中国ILuvU";
	char c = 'c';
	unsigned char uc = 'd';
	short sNum = 123;
	unsigned short usNum = 777;
	std::string str = "777【手机版魔域】法宝系统-法宝的绑定规则123";
	int nNum = 189459045;
	unsigned int unNum = 30678768;
	long lNum = 456990345;
	unsigned long ulNum = 90345945;
	long long llNum = 234567983434;
	unsigned long long ullNum = 89456895765689;
	float x = 7.5f;
	double y = 6.666666666;
	long double z = 7.7777777777777;
	
	bs.SetAction(1);
	bs.Dump();
	bs << a;
	bs.Dump();
	bs << szText;
	bs.Dump();
	bs << c;
	bs.Dump();
	bs << uc;
	bs.Dump();
	bs << sNum;
	bs.Dump();
	bs << usNum;
	bs.Dump();
	bs << str;
	bs.Dump();
	bs << nNum;
	bs.Dump();
	bs << unNum;
	bs.Dump();
	bs << lNum;
	bs.Dump();
	bs << ulNum;
	bs.Dump();
	bs << llNum;
	bs.Dump();
	bs << ullNum;
	bs.Dump();
	bs << x;
	bs.Dump();
	bs << y;
	bs.Dump();
	bs << z;
	bs.Dump();

	bool a2;
	char szText2[32];
	char c2;
	unsigned char uc2;
	short sNum2;
	unsigned short usNum2;
	std::string str2;
	int nNum2;
	unsigned int unNum2;
	long lNum2;
	unsigned long ulNum2;
	long long llNum2;
	unsigned long long ullNum2;
	float x2;
	double y2;
	long double z2;

	bs >> a2 >> szText2 >> c2 >> uc2 >> sNum2 >> usNum2 >> str2 >> nNum2 >> unNum2 >> lNum2 >> ulNum2 >> llNum2 >> ullNum2 >> x2 >> y2 >> z2;

	bool bEqual = (a == a2) && (strcmp(szText, szText2) == 0) && (c== c2) && (uc == uc2) && (sNum == sNum2) && (usNum == usNum2)
		&& (str == str2) && (nNum == nNum2) && (unNum == unNum2) && (lNum == lNum2) && (ulNum == ulNum2) && (llNum == llNum2) 
		&& (ullNum == ullNum2) && (x == x2) && (y == y2) && (z == z2);
	printf("%s\n", bEqual? "Equal" : "Not Equal");

	bs.Dump();
}

int main()
{
	TestServerSync();
	TestServerAsync();

	return 0;
}